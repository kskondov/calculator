$(document).ready(function() {

    var number = "";
    var newNumber = "";
    var total = "";
    var operator = "";
    var totalDiv = $("#total");
    var expression = $("#expression");
    var regex = new RegExp("[0-9 \+\\-\*\=/\.\^[\r\n]");
    var equalsWasPressed = false;

    totalDiv.text("0");

    $("#sqrt").text(String.fromCharCode(0x221A));
    $("#negative").text(String.fromCharCode(0xB1));

    $(".numbers").click(function() {
        numberate($(this).text());
        expression.text(total);
    });

    $(".operators").click(function() {
        operate($(this).text());
        expression.text(total);
    });

    $("#sqrt").click(sqrt);

    $("#equals").click(equals);

    $("#clear").click(clear);

    $("#clearAll").click(clearAll);

    $("#negative").click(makeNegative)

    $(document).keypress(function(event) {
        var keyCode = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(keyCode)) {
            totalDiv.text("NaN");
        } else {
            switch (keyCode) {
                case "1":
                    numberate("1");
                    break;
                case "2":
                    numberate("2");
                    break;
                case "3":
                    numberate("3");
                    break;
                case "4":
                    numberate("4");
                    break;
                case "5":
                    numberate("5");
                    break;
                case "6":
                    numberate("6");
                    break;
                case "7":
                    numberate("7");
                    break;
                case "8":
                    numberate("8");
                    break;
                case "9":
                    numberate("9");
                    break;
                case "0":
                    numberate("0");
                    break;
                case ".":
                    numberate(".");
                    break;
                case "*":
                    operator = "*";
                    operate("*");
                    break;
                case "+":
                    operator = "+";
                    operate("+");
                    break;
                case "-":
                    operator = "-";
                    operate("-");
                    break;
                case "/":
                    operator = "/";
                    operate("/");
                    break;
                case "=":
                case String.fromCharCode(0x0D):
                    equals();
                    break;
            }
        }
    });

    function sqrt() {
        if (equalsWasPressed) {
            number = newNumber;
        }

        total = total.substring(0, total.length - number.length);
        var sqrt = (Math.sqrt(number).toFixed(8));
        newNumber = parseFloat(sqrt).toString();
        total += parseFloat(newNumber);
        expression.text(total);
        totalDiv.text(total);
        testNumLength(total);
        number = newNumber;
    }

    function clear() {
        clearLastChar();
        if (total.length === 0 || total === "0") {
            total = "";
            totalDiv.text("0");
        } else {
            totalDiv.text(total);
        }
        expression.text(total);
        number = "";
    }

    function clearAll() {
        number = "";
        newNumber = "";
        total = "";
        totalDiv.text("0");
        expression.text("Expression");
    }

    function testNumLength(number) {
        if (number.length > 9) {
            totalDiv.text(number.substr(number.length - 9, 9));
        }
    }

    function numberate(digit) {
        if (equalsWasPressed === true) {
            equalsWasPressed = false;
            number = "";
            total = "";
            operator = "";

            if (digit === ".") {
                number = newNumber.toString() + digit;
                total += number;
                totalDiv.text(total);
            } else {
                number += digit;
                total += number;
                totalDiv.text(total);
                testNumLength(number);
            }
        } else {
            if (digit === "." && number.includes(".")) {
                digit = "";
            }
            if (digit === "." && number === "") {
                number = "0";
                total += number;
            }

            if (digit === "0" && number === "0") {
                digit = "";
            } else {
                if (number === "0" && digit !== ".") {
                    number = digit;
                    total = total.substring(0, total.length - 1) + number;
                } else {
                    number += digit;
                    total += digit;
                }
            }

            totalDiv.text(total);
            testNumLength(total);
        }
    }

    function operate(command) {
        if (total.length === 0 && command === "-") {
            total += command;
            totalDiv.text(total);
        }

        // if (total.charAt(total.length - 1) === ".") {
        //     clearLastChar();
        // }

        equalsWasPressed = false;
        checkForOperators();
        operator = command;
        newNumber = number;

        total += operator;
        if ($.isNumeric(total.charAt(total.length - 1))) {
            total += operator;
        } else {
            total = (total.substring(0, total.length - 1) + operator);
        }

        totalDiv.text(total);
        testNumLength(total);
        number = "";
    }


    function lastCharIsOperator() {
        return total.charAt(total.length - 1) === "+" || total.charAt(total.length - 1) === "-" || total.charAt(total.length - 1) === "*" || total.charAt(total.length - 1) === "/";
    }

    function checkForOperators() {
        while (lastCharIsOperator()) {
            clearLastChar();
        }
    }

    function clearLastChar() {
        total = total.substring(0, total.length - 1);
    }

    function makeNegative() {
        var negative = "-";
        if (lastCharIsOperator()) {

            return;
        }

        if (equalsWasPressed) {
            number = newNumber.toString();
            equalsWasPressed = false;
        }

        if (total.charAt(total.length - 1) === ".") {
            clearLastChar();
        }

        if (parseFloat(number) < 0) {
            total = total.substring(0, total.length - number.length);
            number = number.substring(1, number.length);
            total += number;

        } else {
            if (parseFloat(totalDiv.text()) === 0) {
                return;
            } else {
                total = total.substring(0, total.length - number.length);
                negative += number;
                if (operator === "-") {
                    number = "(" + negative + ")";
                }else{
                    number = negative;
                }
                
                total += number;
            }

        }
        expression.text(total);
        totalDiv.text(total);
        testNumLength(total);
    }

    function equals() {
        equalsWasPressed = true;
        checkForOperators();
        if (total === "") {
            total = "0";
        }
        if (total.charAt(total.length - 1) === ".") {
            clearLastChar();
        }

        var result = String(parseFloat(eval(total).toFixed(8)));
        expression.text(total + " = " + parseFloat(result));

        if (result === Infinity) {
            alert("cannot divide by zero");
            totalDiv.text("Error");
        }
        if (result.length > 10) {
            totalDiv.text(result.substring(0, 7) + "+e");
        } else {
            totalDiv.text(parseFloat(result));
        }

        number = "";
        newNumber = parseFloat(result);
        total = newNumber.toString();
    }
});